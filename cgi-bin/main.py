import sys

class SQL_Select_Generator:
    def __init__(self, table, selectArgs):
        self.table = table
        self.selectArgs = selectArgs

    def generate(self):
       return 'select ' + self.selectArgs + ' from \"' + self.table + '\";'  # + '\" where location = \'' + self.area + '\' and date >= \'' + self.date +"\';"

class SQL_Join_Generator:
    def __init__(self, table1, table2, joinType, joinCond):
        self.table1 = table1
        self.table2 = table2
        self.joinType = joinType
        self.joinCond = joinCond

    def generate(self):
        pieces = self.joinCond.split(',')
        str = "select * from " + self.table1 + " " + self.joinType + " "  + self.table2 + " on "
        for piece in pieces:
            str += self.table1 + "." + piece + " = " + self.table2 + "." + piece
            if piece != pieces[len(pieces) - 1]:
                str += " and "
            else:
                str += ";"
        return str;

class SQL_Union_Generator:
    def __init__(self, table1, table2, unionType, selectArgs):
        self.table1 = table1
        self.table2 = table2
        self.unionType = unionType
        self.selectArgs = selectArgs

    def generate(self):
        s1 = SQL_Select_Generator(self.table1, self.selectArgs)
        sql1 = s1.generate()
        sql1 = sql1[:-1]
        s2 = SQL_Select_Generator(self.table2, self.selectArgs)
        sql2 = s2.generate()
        return sql1 + " " + self.unionType + " " + sql2

class SQL_Utility_Generator:
    def __init__(self, table):
        self.table = table

    def generate(self):
        return "select column_name from information_schema.columns where table_name=\'" + self.table + "\';"

class SQL_Join_Utility_Generator:
    def __init__(self, table, table2):
        self.table = table
        self.table2 = table2

    def generate(self):
        return "select column_name from information_schema.columns where table_name=\'" + self.table + "\' and column_name in (select column_name from information_schema.columns where table_name=\'" + self.table2 + "\');"


def main(argv):
    sqlType = argv[0]
    mainTable = argv[1]
    s = None
    if sqlType == 'union':
        secondTable = argv[2]
        unionType = argv[3]
        selectArgs = argv[4]
        s = SQL_Union_Generator(mainTable, secondTable, unionType, selectArgs)
    elif sqlType == 'join':
        secondTable = argv[2]
        joinType = argv[3]
        joinArgs = argv[4]
        s = SQL_Join_Generator(mainTable, secondTable, joinType, joinArgs)
    elif sqlType == 'select':
        selectArgs = argv[2]
        s = SQL_Select_Generator(mainTable, selectArgs)
    elif sqlType == 'utility':
        s = SQL_Utility_Generator(mainTable)
    elif sqlType == 'joinUtil':
        secondTable = argv[2]
        s = SQL_Join_Utility_Generator(mainTable, secondTable)
    else:
        print "ERROR"
    if s is not None:
        sql = s.generate()
        print sql
    else:
        print "ERROR"

if __name__ == "__main__":
    main(sys.argv[1:])