CREATE TABLE IF NOT EXISTS elevation
	(id int, date date, location varchar(55))
	;
		
		INSERT INTO elevation
			(id, date, location)
			VALUES
				(1, '2015-1-1', 'Delaware'),
				(2, '2015-10-1', 'Pennsylvania'),
				(3, '2015-8-18', 'New Jersey'),
				(4, '2015-4-23', 'Pennsylvania'),
				(5, '2015-12-23', 'Delaware'),
				(6, '2015-6-23', 'New Jersey'),
				(7, '2015-2-23', 'Pennsylvania'),
				(8, '2014-12-23', 'Pennsylvania'),
				(9, '2015-8-24', 'New Jersey')
				;
CREATE TABLE IF NOT EXISTS "coremass"
	(id int, date date, location varchar(55))
	;
		
		INSERT INTO "coremass"
			(id, date, location)
			VALUES
				(1, '2015-11-1', 'Delaware'),
				(2, '2015-11-1', 'Pennsylvania'),
				(3, '2015-8-17', 'New Jersey'),
				(4, '2015-4-26', 'Pennsylvania'),
				(5, '2015-5-12', 'Delaware'),
				(6, '2015-3-23', 'New Jersey'),
				(7, '2015-2-23', 'Pennsylvania'),
				(8, '2014-12-23', 'Pennsylvania')
				;
CREATE TABLE IF NOT EXISTS "waterquality"
	(id int, date date, location varchar(55))
	;
		
		INSERT INTO "waterquality"
			(id, date, location)
			VALUES
				(1, '2015-11-1', 'Delaware'),
				(2, '2015-11-1', 'Pennsylvania'),
				(3, '2015-8-18', 'New Jersey'),
				(4, '2015-4-23', 'Pennsylvania'),
				(5, '2015-5-23', 'Delaware'),
				(6, '2015-6-23', 'New Jersey'),
				(7, '2015-2-23', 'Pennsylvania')
				;
